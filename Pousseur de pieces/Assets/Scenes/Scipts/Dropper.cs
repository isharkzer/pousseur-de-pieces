using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class Dropper : MonoBehaviour

{
    [SerializeField]
    private GameObject coin;
    public void spawnCoin()
    {
        GameObject newcoin = Instantiate(coin, transform.position - new Vector3(-0.40f, 0.6f, UnityEngine.Random.Range(-0.5f,0.5f)), Quaternion.Euler(0, 0, 90));
        newcoin.GetComponent<Rigidbody>().AddForce(0, UnityEngine.Random.Range(-6.5f,-3.5f), 0);
        Debug.Log("New Coin");
    }

}


