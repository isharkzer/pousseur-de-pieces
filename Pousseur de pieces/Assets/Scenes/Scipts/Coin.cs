using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField]
    private AudioClip bigcoin;

    [SerializeField]
    private AudioClip midcoin;

    [SerializeField]
    private AudioClip littlecoin;

    private AudioSource source;

    private bool pousse;

    private void Start()
    {
        source = GetComponent<AudioSource>();
        pousse = true;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.GetComponent<Coin>() != null)
        {
            source.clip = midcoin;
            source.Play();
        }

        if (collision.collider.name == "PoussePousse" && pousse)
        {
            source.clip = bigcoin;
            source.Play();
            pousse = false;
        }
    }
}
